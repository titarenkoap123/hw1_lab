const fs = require('fs');
const path = require('path');
const filesFolder = 'files/';
const extensions = ['.txt', '.js', '.log', '.json', '.yaml', '.xml'];
let objPasswords = {};
const JsonFile = 'fileWithPasswords.json';
const passwordRegEx = /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d).{8,}$/;

function createFile (req, res, next) {
  const fileName = req.body.filename;
  const fileContent = req.body.content;
  if(req.body.password && passwordRegEx.test(req.body.password) || !req.body.password) {
    if (extensions.includes(path.extname(fileName))) {
        fs.appendFile(path.resolve(filesFolder, fileName), fileContent, {flags: 'a'}, (err) => {
          if(err) throw err;
            if (!fs.existsSync(JsonFile)) {
              objPasswords[fileName] = req.body.password;
              fs.writeFile(JsonFile, JSON.stringify(objPasswords), function(err) {
                if(err) throw err;
              });
            } else {
              fs.readFile(JsonFile, function(err, data) {
                if(err) throw err;
                objPasswords = JSON.parse(data);
                objPasswords[fileName] = req.body.password;
                fs.writeFile(JsonFile, JSON.stringify(objPasswords), function(err) {
                  if(err) throw err;
                });
              })
            }
        });
        res.status(200).send({ "message": "File created successfully" });
    } else {
      res.status(400).send({ "message": "Wrong type of extension" });
    }
  } else {
    res.status(400).send({ "message": "Invalid password" });
  }
}

function getFiles (req, res, next) {
  res.status(200).send({
    "message": "Success",
    "files": fs.readdirSync(filesFolder)});
}

const getFile = (req, res, next) => {
  fs.readFile(JsonFile, function(err, data) {
    if(err) throw err;
    let obj = JSON.parse(data);
    let key = obj[req.params.filename];
    if(key && key === req.query.password || !key) {
      if(fs.readdirSync(filesFolder).includes(req.params.filename)) {
        res.status(200).send({
          "message": "Success",
          "filename": req.params.filename,
          "content": fs.readFileSync(path.resolve(filesFolder, req.params.filename), 'utf-8'),
          "extension": path.extname(req.params.filename).split('.')[1],
          "uploadedDate": fs.statSync(path.resolve(filesFolder, req.params.filename)).birthtime
        });
      } else {
        res.status(400).send({"message": `No file with ${req.params.filename} filename found`});
      }
    } else {
      res.status(400).send(`Password incorrect`);
    }
  })
}

const editFile = (req, res, next) => {
  fs.readFile(JsonFile, function(err, data) {
    if(err) throw err;
    let obj = JSON.parse(data);
    let key = obj[req.body.filename];
    if(key && key === req.body.password || !key) {
      fs.unlinkSync(path.resolve(filesFolder, req.body.filename));
      fs.appendFile(path.resolve(filesFolder, req.body.filename), req.body.content, {flag: 'a'}, (err) => {
        if(err) throw err;
      });
      res.status(200).send({
        "message": "File edited successfully"
      })
    } else {
      res.status(400).send(`Password incorrect`);
    }
  })

}

const deleteFile = (req, res, next) => {
  fs.readFile(JsonFile, function(err, data) {
    if(err) throw err;
    let obj = JSON.parse(data);
    let key = obj[req.params.filename];
    if(key && key === req.query.password || !key) {
      delete obj[req.params.filename];
      fs.writeFile(JsonFile, JSON.stringify(obj), function(err) {
        if(err) throw err;
      });
      fs.unlinkSync(path.resolve(filesFolder, req.params.filename));
      res.status(200).send("File was deleted");
    } else {
      res.status(400).send(`Password incorrect`);
    }
  })
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile
}
